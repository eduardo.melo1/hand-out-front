import React from 'react';
import Login from './Pages/Login'
import Register from './Pages/Register'
import RecoverPassword from './Pages/RecoverPassword';
import ActivateAccountDonor from './Pages/ActivateAccountDonor'
import ActivateAccountInstituion from './Pages/ActivateAccountInstitution';
import Home from './Pages/Home';
import HomeInstitution from './Pages/Institution/Home'
import NewNecessity from './Pages/Institution/NewNecessity'
import ShowNecessity from './Pages/Institution/ShowNecessity'
import MyAccount from './Pages/Institution/MyAccount'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import NewPassword from './Pages/NewPassword';

import { createTheme, ThemeProvider } from '@mui/material/styles';
import Map from './Pages/Main/Map';
import RoundDiv from './Components/RoundDiv';
import LogoutPage from './Pages/LogoutPage';
import InstitutionDetails from './Pages/InstitutionDetails';
import Favorites from './Pages/Favorites';
import MyAccountUser from './Pages/MyAccount';

const customTheme = createTheme({
  palette : {
    primary: {
      light: '#FDC97A',
      main: '#000000',
      dark: '#B16E09',
      contrastText: '#FFFFFF',
      complementaryColor: '#A9A9A9'
    },
    error: {
      light: '#E57373',
      main: '#F44336',
      dark: '#D32f2F',
      contrastText: '#FFFFFF',
    },
    text: {
      grey: '#A1A1A1',
      ligth: '#F8F8F8',
      black: '#000000',
      white: '#FFFFFF',
    },
    background: {
      default: '#F4F5F7',
      button: '#F5F6FA'
    },
  },
  components: {
    MuiGrid: {
      styleOverrides: {
        root: {
          display: 'flex',
          flexDirection: 'column',
          
          width: '100%',
        },
      },
    },
    MuiCard: {
      styleOverrides: {
        root: {
          display: 'flex',
          flexDirection: 'row',
          
          width: '80%',
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        h5: {
          fontWeight: 'bold',
        }
      }
    }
  },
});

function App() {
  return (
    <ThemeProvider theme={customTheme}>
      <Router>
        <div className="App">
          <div className="content">
            <Switch>
              <Route component={Home} path='/' exact />
              <Route component={Login} path='/login' exact />
              <Route component={LogoutPage} path='/logout' exact />
              <Route component={Register} path='/register' exact />
              <Route component={RecoverPassword} path='/recover' exact />
              <Route component={NewPassword} path='/recover/verify/:token' exact />
              <Route component={ActivateAccountDonor} path='/register/verify/donor/:token' exact />
              <Route component={ActivateAccountInstituion} path='/register/verify/institution/:token' exact />
              <Route component={HomeInstitution} path='/homeInstitution' exact />
              <Route component={NewNecessity} path='/newNecessity' exact />
              <Route component={InstitutionDetails} path='/show-institution/:id_inst' exact />
              <Route component={ShowNecessity} path='/showNecessity' exact />
              <Route component={MyAccount} path='/myAccount' exact />
              <Route component={Map} path='/map' exact />
              <Route component={Favorites} path='/favorites' exact />
              <Route component={MyAccountUser} path='/meuCadastro' exact />
            </Switch>
          </div>
        </div>
      </Router>
    </ThemeProvider> 
  )
}

export default App;
