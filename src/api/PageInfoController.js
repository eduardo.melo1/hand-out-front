import { server, handler_util } from './index'
import { decodeToken } from 'react-jwt'

export async function institutionData({ ...params }) {
    const { data } = await handler_util(server.get(`/show-institution/${params.id}`))

    return data.err? data.err.response : data;
}

export async function getInstitutions({ ...params }) {
    const { data } = await handler_util(server.get(`/show-institutions`))

    return data.err? data.err.response : data;
}

export async function getFavorites(id) {
    const { data } = await handler_util(server.get(`favorites/${id}`))

    return data.err? data.err.response : data.data;
}

export async function favoriteInstitution(id, id_inst) {
    const { data } = await handler_util(server.post(`favorite/${id}/${id_inst}`))

    return data.err? data.err.response : data;
}

export async function unfavoriteInstitution(id, id_inst) {
    const { data } = await handler_util(server.delete(`delete-favorite/${id}/${id_inst}`))

    return data.err? data.err.response : data;
}

