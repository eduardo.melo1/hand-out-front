import axios from 'axios'

export const server =  axios.create({
  baseURL: process.env.REACT_APP_API_URL
})

// Set the AUTH token for any request
server.interceptors.request.use(function (config) {
  const token = localStorage.getItem('token');
  config.headers.Authorization =  token ? `Bearer ${token}` : '';
  return config;
});

export async function handler_util(_promise) {
    try {
      return await _promise
    } catch (e) {
      return {
        data: { err: e },
        status: 0,
        statusText: undefined,
        headers: undefined,
        config: e.config,
        request: e.request,
      }
    }
  }