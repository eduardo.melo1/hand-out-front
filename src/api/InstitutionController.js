import { server } from './index'

export async function NewNecessity({ ...params }) {
    const { data } = await server.post('/new-necessity', params)
    return data;
}

export async function ShowNecessity() {
    const { data } = await server.get('/show-necessities')
    return data.data.necessities
}

export async function ShowCategories() {
    const { data } = await server.get('/show-categories')
    return data.data.categories
}

export async function Enablenecessity(id, necessity) {
    const { data } = await server.patch(`/show-necessities/${id}/enable-necessity/${necessity}`)
    return data;
}

export async function Disablenecessity(id, necessity) {
    const { data } = await server.patch(`/show-necessities/${id}/disable-necessity/${necessity}`)
    return data;
}

export async function UpdateInstitution({ ...params }) {
    const { data } = await server.patch(`/change-institution`, params)
    return data;
}

export async function ActivateInstitution(token, { ...params }) {
    const { data } = await server.patch(`${token}`, params)
    return data;
}


