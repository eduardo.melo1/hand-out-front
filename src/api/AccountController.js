import { server, handler_util } from './index'
import { decodeToken } from 'react-jwt'

export async function Logout(history = false) {
    localStorage.clear()
    localStorage.removeItem('token');
    server.defaults.headers.Authorization = undefined;
    if (history !== false) history.push('/login')
}

export async function LoginAcc({ ...params }) {
    const { data, data: {token} } = await handler_util(server.post('/login', params))
    if(token){
        localStorage.setItem('token', JSON.stringify(token));
    }
    return data.err? data.err.response : data;
}

export async function VerifyAccountDonor(route) {
    return await handler_util(server.patch(route))
}

export async function setAuthToken(token, isInstituicao) {
    localStorage.setItem('token', token);
    localStorage.setItem('isInstituicao', isInstituicao);
    server.defaults.headers.Authorization = `Bearer ${token}`
}

export function IsLogged() {
    return decodeToken(localStorage.token) !== null;
}

export function isInstituicao() {
    return localStorage.getItem('isInstituicao');
}

export async function myAccount() {
    const { data } = await handler_util(server.get('/my-account'))
    return data
}

export async function RegisterAcc({ ...params }) {
    const { data } = await handler_util(server.post('/register', params))

    return data.err? data.err.response : data;
}

export async function requestPwdRecovery({ ...params }) {
    server.post('/recover', params);
}

export async function setNewPwd(route, { ...params }) {
    const { data } = await handler_util(server.patch(route, params));
    
    return data.err? data.err.response : data;
}

export function getUserId() {
    return decodeToken(localStorage.getItem('token')).userLoginId
}

export async function changePWD({ ...params }) {
    const { data } = await handler_util(server.patch('/change-password', params));

    return data.err? data.err.response : data;
}
