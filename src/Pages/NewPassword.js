import React, { useEffect, useState } from 'react';
import ValidationFormsContainer from '../Components/ValidationFormsContainer';
import NewPwdForm from '../Components/NewPwdForm';
import { useHistory } from 'react-router-dom';
import { IsLogged, setNewPwd } from '../api/AccountController';

import '../Styles/EntryPages.css'
import Modal from '../Components/Modal';

function NewPassword() {
    const history = useHistory();

    const NewPwd = async details => {
        const response = await setNewPwd(window.location.pathname, {...details});

        console.log(response)
        if(!(response.status && response.status >= 400))
            handleClickOpen();

        return response;
    }

    useEffect(() => {
        document.title = 'Confirmação de conta';

        if(IsLogged())
            history.push('/');
    });

    //dialog setings
    const title = 'Sua senha foi atualizada';
    const text = 'Agora é só retornar ao login e acessar sua conta!';
    const btnText = 'Retornar ao Login';

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        history.push('/login');
    };

    return (
        <div class="page">
            <ValidationFormsContainer></ValidationFormsContainer>
            <NewPwdForm NewPwd={NewPwd}></NewPwdForm>
            <Modal 
                title={title} 
                text={text}
                btnText={btnText}
                open={open}
                handleClose={handleClose}>
            </Modal>
        </div>
    )
}

export default NewPassword
