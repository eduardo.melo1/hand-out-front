import React, { useEffect } from 'react'
import ValidationFormsContainer from '../Components/ValidationFormsContainer';
import LoginForm from '../Components/LoginForm';
import { IsLogged, isInstituicao, LoginAcc, setAuthToken } from '../api/AccountController';

import '../Styles/EntryPages.css'
import { useHistory } from 'react-router';

function Login() {
    const history = useHistory();

    useEffect(() => {
        document.title = 'Login';

        if(IsLogged() && isInstituicao())
            history.push('/homeInstitution');
        else if(IsLogged())
            history.push('/map');
    });

    const Login = async details => {
        const response = await LoginAcc({...details});

        if(!response || response.status >= 400){
            console.log(response)
        }else {
            setAuthToken(response.data.token, response.data.isInstituicao).then(
                response.data.isInstituicao ? history.push('/homeInstitution') : history.push('/map')                
            )
        }

        return response;
    }

    return (
        <div class="page">
            <ValidationFormsContainer></ValidationFormsContainer>
            <LoginForm Login={Login}></LoginForm>
        </div>
    )
    
}

export default Login
