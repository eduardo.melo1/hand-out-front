const styles = theme => ({  
    paperRoot: {
      paddingLeft: theme.spacing(3),
      paddingRight: theme.spacing(3),
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2),
      boxShadow: '0px 2px 2px 0px rgba(0,0,0,0.15)',
      height: '100%',
    },
    gridHeader: {
      borderEndStartRadius: '35%',
      borderEndEndRadius: '35%',
      height: '22vh',
      boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
      backgroundImage: 'linear-gradient(#FF9900, #FDC97A)',
      display: 'flex',
    },
    sectionHeader: {
      marginBottom: theme.spacing(4)
    },
    sectionButton: {
      marginTop: theme.spacing(5)
    },
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    margin: {
      margin: theme.spacing(1),
    },
    withoutLabel: {
      marginTop: theme.spacing(3),
    },
    formControl: {
      marginRight: theme.spacing(1),
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    marginTop: {
      marginTop: theme.spacing(4),
    },
    buttonControl: {
      boxShadow: '0px 2px 2px 0px rgba(0,0,0,0.15)',
    }
  })
  
  export default styles;