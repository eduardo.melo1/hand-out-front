import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { isInstituicao, IsLogged, Logout, myAccount } from '../../../api/AccountController'
import { ShowCategories } from '../../../api/InstitutionController'
import { NewNecessity } from '../../../api/InstitutionController'
import '../../../Styles/FormStyles.css'

//MUI COMPONENTS
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import Select from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import { makeStyles } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert'
import Snackbar from '@material-ui/core/Snackbar'

//STYLES
import styles from './style'

//ICONS
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import CreateIcon from '@material-ui/icons/Create'
import VisibilityIcon from '@material-ui/icons/Visibility'
import PersonIcon from '@material-ui/icons/Person'

//COMPONENTS
import GridHeader from '../../../Components/GridHeader'
import RoundDiv from '../../../Components/RoundDiv';

const useStyles = makeStyles({
    gridHeader: {
        borderEndStartRadius: '35%',
        borderEndEndRadius: '35%',
        height: '22vh',
        boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
        backgroundImage: 'linear-gradient(#FF9900, #FDC97A)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    marginTop: {
        marginTop: '10%',
        justifyContent: 'left',
        alignItems: 'left',
    },
    marginItems: {
        marginTop: '5%',
        marginBottom: '5%',
        
    },
    title: {
      textAlign: 'center'
    },
    defaultGrid: {
      marginTop: '10%',
      margin: 'auto',
      width:'80%',
      display: 'flex',
      flexDirection: 'column',
      alignContent: 'center',
      alignItems: 'center',
      textAlign:'center',
  }
    
})

function ActivateAccount() {
  const classes = useStyles()
  const history = useHistory();

  const [errorName, setErrorName] = useState(false)
  const [textErrorName, setTextErrorName] = useState('')
  const [errorSelect, setErrorSelect] = useState(false)
  const [textErrorSelect, setTextErrorSelect] = useState('')

  const [categories, setCategories] = useState('')

  const [nameNecessity, setNameNecessity] = useState('')
  const [categoria, setCategoria] = useState("")
  const [name, setName] = useState("")
  const [city, setCity] = useState("")

  const [loading, setLoading] = useState(false)

  const [err, setErr] = useState(false)
  const [errText, setErrText] = useState("")
  const [success, setSuccess] = useState(false)
  const [successText, setSuccessText] = useState("")

  /* alerta de sucesso ou erro */
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const handleCloseErr = (event) => {
    setErr(false);
  };
  const handleCloseSucces = (event) => {
    setSuccess(false);
  };
  
  const handleName = (event) => {
    setNameNecessity(event.target.value);
  }

  const handleCategoria = (event) => {
    setCategoria(event.target.value);
  }

  useEffect(() => {
    document.title = 'Cadastrar Necessidade';

    if(!IsLogged())
      history.push('/login')

    if(!isInstituicao())
      history.push('/login')

    setLoading(true)
    myAccount().then(value =>{
      if(value){
        setName(value.data.userData.nome)
        setCity(value.data.userData.addresses[0].cidade)
      }
    })
    ShowCategories().then(value => {
      setCategories(value)
    })
    setLoading(false)

    
  }, [loading])

  function handleLogout (){
    Logout(history);
  }

  function handleSubmit (){

    setLoading(true)

    setErrorName(false)
    setTextErrorName('')
    setErrorSelect(false)
    setTextErrorSelect('')

    if(nameNecessity===''){
      setErrorName(true)
      setTextErrorName('Campo Obrigatório')
    }
    if(categoria === ''){
      setErrorSelect(true)
      setTextErrorSelect('Campo Obrigatório')
    }
    if(!errorName || !errorSelect){
      const params = {
        nome: nameNecessity,
        descricao: 'teste',
        id_categoria: categoria
      }
      NewNecessity({ ...params }).then(value => {
        setNameNecessity('')
        setCategoria('')
        setSuccessText("Necessidade Cadastrada com Sucesso")
        setSuccess(true)
        setLoading(false)
      })
    }

    
  }

  return (
    <Grid>
         {
            !loading &&
              <GridHeader 
                  name={name}
                  city={city}
                  back={true}
              />
          }

        <Grid className={classes.marginTop}>
          
            <Grid item lg={12} md={12} sm={12} xs={12} className={classes.title}>
                <Typography 
                    variant="h5" 
                    color="text.black"
                >
                    Cadastrar Necessidade
                </Typography>
            </Grid>
           <Grid item lg={12} md={12} sm={12} xs={12}>
              <Container className={classes.defaultGrid}>
                <TextField 
                  sx={{ m: 1, minWidth: '80%' }}
                  variant="outlined" 
                  label="Nome"
                  value={nameNecessity}
                  required
                  onChange={handleName}
                  error={errorName}
                  helperText={textErrorName}
                />
                <FormControl 
                  sx={{ m: 1, minWidth: '80%' }}
                  error={errorSelect}
                  required
                >
                  <InputLabel>Categoria</InputLabel>
                  <Select
                    id="categoria"
                    value={categoria}
                    label="Categoria"
                    onChange={handleCategoria}
                    fullWidth
                  >
                    {
                      categories.length ? 
                        categories.map(c => (
                          <MenuItem value={c._id}>{c.tipo}</MenuItem>
                        ))
                      :
                        null
                    }
                  </Select>
                  {
                    errorSelect ? 
                      <FormHelperText>{textErrorSelect}</FormHelperText>
                    :
                      null
                  }
                </FormControl>
              </Container>
              <br/>
              <input disabled={loading} className="submit" type="submit" value="Cadastrar" onClick={handleSubmit} />
           </Grid>

           <Grid item >
             
           </Grid>
            
            
        </Grid>

        <Snackbar open={success} autoHideDuration={6000} onClose={handleCloseSucces}>
          <Alert icon={false}  onClose={handleCloseSucces} severity="success">
            {successText}
          </Alert>
        </Snackbar>
      
        <Snackbar open={err} autoHideDuration={6000} onClose={handleCloseErr}>
          <Alert icon={false} onClose={handleCloseErr} severity="error">
            {errText}
          </Alert>
        </Snackbar>


    </Grid> 
    
  )
}

export default ActivateAccount
