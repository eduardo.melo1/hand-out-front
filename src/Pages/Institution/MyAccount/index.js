import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { isInstituicao, IsLogged, myAccount } from '../../../api/AccountController'
import { UpdateInstitution } from '../../../api/InstitutionController'

//MUI COMPONENTS
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { makeStyles } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import MuiAlert from '@material-ui/lab/Alert'
import Snackbar from '@material-ui/core/Snackbar'

//COMPONENTS
import GridHeader from '../../../Components/GridHeader'

//ICONS
import AddIcon from '@material-ui/icons/Add'

const useStyles = makeStyles({
  gridHeader: {
      borderEndStartRadius: '35%',
      borderEndEndRadius: '35%',
      height: '22vh',
      boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
      backgroundImage: 'linear-gradient(#FF9900, #FDC97A)',
      justifyContent: 'center',
      alignItems: 'center',
  },
  marginTop: {
      marginTop: '10%',
      justifyContent: 'left',
      alignItems: 'left',
  },
  marginItems: {
      marginTop: '5%',
      marginBottom: '5%',
      
  },
  title: {
    textAlign: 'center'
  },
  defaultGrid: {
    marginTop: '10%',
    margin: 'auto',
    width:'80%',
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    textAlign:'center',
  },
  modalGrid: {
    marginTop: '10%',
    width:'100%',
    display: 'flex',
    flexDirection: 'row',
  },
  button: {
    width:'100%',
  }
})

function ActivateAccount() {
  
  const classes = useStyles()
  const history = useHistory()

  const [errorContact, setErrorContact] = useState([])
  const [errorHorario, setErrorHorario] = useState([])
  const [errorEndereco, setErrorEndereco] = useState([])

  const [name, setName] = useState("")
  const [city, setCity] = useState("")
  const [email, setEmail] = useState("")

  const [contacts, setContacts] = useState([])
  const [enderecos, setEnderecos] = useState([])
  const [horarios, setHorarios] = useState([])

  const [loading, setLoading] = useState(false)

  const [err, setErr] = useState(false)
  const [errText, setErrText] = useState("")
  const [success, setSuccess] = useState(false)
  const [successText, setSuccessText] = useState("")

  const [openContact, setOpenContact] = React.useState(false);
  const [openEndereco, setOpenEndereco] = React.useState(false);
  const [openHorario, setOpenHorario] = React.useState(false);
  
  const [scroll, setScroll] = React.useState('paper');

  /* alerta de sucesso ou erro */
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const handleCloseErr = (event) => {
    setErr(false);
  };
  const handleCloseSucces = (event) => {
    setSuccess(false);
  };

  /* abrir modal de cada tipo */
  const handleClickOpen = (scrollType, type) => () => {
    setScroll(scrollType);
    if(type === 'contato')
      setOpenContact(true);
    else if(type === 'endereco')
      setOpenEndereco(true);
    else if(type === 'horario')
      setOpenHorario(true);
  };

  /* fechar modais */
  const handleClose = () => {
    handleClearValidation()
    setOpenContact(false);
    setOpenEndereco(false);
    setOpenHorario(false);
  };

  /* sempre abrir o modal no comeco */
  const descriptionElementRef = React.useRef(null);

  useEffect(() => {
    document.title = 'Home';
    if(!IsLogged())
      history.push('/login')
    if(!isInstituicao())
      history.push('/login')
    setLoading(true)
    myAccount().then(value =>{
      if(value){
        setName(value.data.userData.nome)
        setCity(value.data.userData.addresses[0].cidade)
        setEmail(value.data.userData.email)
        setContacts(value.data.userData.contacts)
        setEnderecos(value.data.userData.addresses)
        setHorarios(value.data.userData.horarios)
      }
    })
    setLoading(false)
    if (openContact || openEndereco || openHorario) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [openContact, openEndereco, openHorario]);

  /* limpar validacao dos fields */
  const handleClearValidation = () => {
    setErrorContact([])
    setErrorEndereco([])
    setErrorHorario([])
    return true
  }

  /* atualizar dados da instituicao */
  const handleSubmitUpdate = (type) => {
    handleClearValidation() 
    
    if(handleValidation(type)){
      const params = {
        telefones: [...contacts],
        enderecos: [...enderecos],
        horarios: [...horarios]
      }
      UpdateInstitution({ ...params }).then(value => {
        setSuccessText("Dados atualizados com Sucesso")
        setSuccess(true)
        setLoading(false)
        history.push('/myAccount')
        handleClose()
      })
      
    }
  }

  /* setar erro e msg de erro para cada item dos vetores caso estejam vazios */
  const handleArrayInputError = (type, typeErr, index, param) => { 
    let hasMatch = false
    var hasErrorMsg = null
    if(type === "contact"){
      for (var i = 0; i < errorContact.length; ++i){
        if(errorContact[i].index === index && errorContact[i][param] !== ''){                          
            if(typeErr === "error") hasMatch = true
            if(typeErr === "text") hasErrorMsg = errorContact[i][param]  
            break
          }
      }
    } 
    else if(type === "horario"){
      for (i = 0; i < errorHorario.length; ++i){
        if(errorHorario[i].index === index && errorHorario[i][param] !== ''){                          
            if(typeErr === "error") hasMatch = true
            if(typeErr === "text") hasErrorMsg = errorHorario[i][param]  
            break
          }
      }
    } 
    else if(type === "endereco"){
      for (i = 0; i < errorEndereco.length; ++i){
        if(errorEndereco[i].index === index && errorEndereco[i][param] !== ''){                          
            if(typeErr === "error") hasMatch = true
            if(typeErr === "text") hasErrorMsg = errorEndereco[i][param]  
            break
          }
      }
    } 
    if(typeErr === "error") return hasMatch
    if(typeErr === "text") return hasErrorMsg  
  }

  /* validação dos campos obrigatorios */
  const handleValidation = (type) => {
    if(type === "contato"){
      let obj = []
      const elem = {
        ddd: "",
        telefone: "",
      }
      contacts.forEach((item, index) => { 
        if(item.ddd==="")
          elem.ddd = "Campo Obrigatório"
        if(item.telefone==="")
          elem.telefone = "Campo Obrigatório"
        if(elem.ddd!=="" || elem.telefone!=="") 
          obj.push({index:index, ddd:elem.ddd, telefone:elem.telefone})
      })
      if(obj.length !== 0){
        setErrorContact([...obj])    
      }  
    }
    if(type === "horario"){
      let obj = []
      const elem = {
        date: "",
        horario_inicio: "",
        horario_fim: ""
      }
      horarios.forEach((item, index) => { 
        if(item.date==="")
          elem.date = "Campo Obrigatório"
        if(item.horario_inicio==="")
          elem.horario_inicio = "Campo Obrigatório"
        if(item.horario_fim==="")
          elem.horario_fim = "Campo Obrigatório"
        if(elem.date!=="" || elem.horario_inicio!=="" || elem.horario_fim!=="") 
          obj.push({index:index, dia:elem.date, inicio:elem.horario_inicio, fim:elem.horario_fim})
      })
      if(obj.length !== 0){
        setErrorHorario([...obj])    
      }  
    }
    if(type === "endereco"){
      let obj = []
      const elem = {
        cep: "",
        rua: "",
        cidade: "",
        numero: "",
        lat: "",
        long: ""
      }
      enderecos.forEach((item, index) => { 
        if(item.cep==="")
          elem.cep = "Campo Obrigatório"
        if(item.rua==="")
          elem.rua = "Campo Obrigatório"
        if(item.cidade==="")
          elem.cidade = "Campo Obrigatório"
        if(item.numero==="")
          elem.numero = "Campo Obrigatório"
        if(item.lat==="")
          elem.lat = "Campo Obrigatório"
        if(item.long==="")
          elem.long = "Campo Obrigatório"
        if(elem.cep!=="" || elem.rua!=="" || elem.cidade!=="" || elem.numero!=="" || elem.lat!=="" || elem.long!=="") 
          obj.push({index:index, cep:elem.cep, rua:elem.rua, city:elem.cidade, num:elem.numero, lat:elem.lat, long:elem.long})
          console.log(obj);
      })
      if(obj.length !== 0){
        setErrorEndereco([...obj])    
      }  
    }
    if(type==="contato")
      return Object.values(errorContact).filter(Boolean).length === 0 
    else if(type==="horario")
      return Object.values(errorHorario).filter(Boolean).length === 0 
    else if(type==="endereco"){
      console.log(Object.values(errorEndereco).filter(Boolean).length === 0 );
      return Object.values(errorEndereco).filter(Boolean).length === 0 
    }
  }

  /* adicionar novo elemento em um dos 3 vetores */
  const handleAddElement = (type) => {
    if(type === 'contato') {
      const newContacts  = [...contacts]
      const elem = {
        ddd: "",
        telefone: "",
      }
      newContacts.push(elem)
      setContacts(newContacts)
    }
    if(type === 'endereco') {
      const newEnderecos  = [...enderecos]
      const elem = {
        cep: "",
        rua: "",
        cidade: "",
        numero: "",
        lat: "",
        long: ""
      }
      newEnderecos.push(elem)
      setEnderecos(newEnderecos)
    }
    if(type === 'horario') {
      const newHorarios  = [...horarios]
      const elem = {
        date: "",
        horario_inicio: "",
        horario_fim: ""
      }
      newHorarios.push(elem)
      setHorarios(newHorarios)
    }
  }

  /* muda valor de cada campo de cada vetor de acordo com o que for digitado */
  const handleChangeValue = (index, field, value) => {
    const newContacts  = [...contacts]
    const newEnderecos = [...enderecos]
    const newHorarios  = [...horarios]
    switch (field) {
      case 'ddd':
        newContacts[index].ddd = value
        setContacts(newContacts)
        break;
      case 'tel':
        newContacts[index].telefone = value
        setContacts(newContacts)
        break;
      case 'cep':
        newEnderecos[index].cep = value
        setEnderecos(newEnderecos)
        break;
      case 'num':
        newEnderecos[index].numero = value
        setEnderecos(newEnderecos)
        break;
      case 'city':
        newEnderecos[index].cidade = value
        setEnderecos(newEnderecos)
        break;  
      case 'rua':
        newEnderecos[index].rua = value
        setEnderecos(newEnderecos)
        break;  
      case 'lat':
        newEnderecos[index].lat = value
        setEnderecos(newEnderecos)
        break;
      case 'long':
        newEnderecos[index].long = value
        setEnderecos(newEnderecos)
        break;
      case 'dia':
        newHorarios[index].date = value
        setHorarios(newHorarios)
        break;
      case 'inicio':
        newHorarios[index].horario_inicio = value
        setHorarios(newHorarios)
        break;
      case 'fim':
        newHorarios[index].horario_fim = value
        setHorarios(newHorarios)
        break;
      default:
        break;
    }
  }

  return (
    <Grid>
         <GridHeader 
            name={name}
            city={city}
            back={true}
        />

        <Grid className={classes.marginTop}>
          
            <Grid item lg={12} md={12} sm={12} xs={12} className={classes.title}>
                <Typography 
                    variant="h5" 
                    color="text.black"
                >
                    Minha Conta
                </Typography>
            </Grid>
           
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <Container className={classes.defaultGrid}>
                <TextField 
                  sx={{ m: 1, minWidth: '80%' }}
                  variant="outlined" 
                  label="Email"
                  value={email}
                  disabled
                />
                
              </Container>
              <br/>
              <input className="submit" type="submit" value="Alterar Endereço" onClick={handleClickOpen('paper', 'endereco')} />
              <br/>
              <input className="submit" type="submit" value="Alterar Contato" onClick={handleClickOpen('paper', 'contato')} />
              <br/>
              <input className="submit" type="submit" value="Alterar Funcionamento" onClick={handleClickOpen('paper', 'horario')} />
           </Grid>
            
            
        </Grid>

        <div>
          <Dialog
            open={openContact}
            onClose={handleClose}
            scroll={scroll}
          >
            <DialogTitle id="scroll-dialog-title">Alteração de Contato Cadastrado</DialogTitle>
            <DialogContent dividers={scroll === 'paper'}>
              <DialogContentText
                id="scroll-dialog-description"
                ref={descriptionElementRef}
                tabIndex={-1}
              >
                {
                  contacts.map( (value, index) => (
                    <Grid className={classes.modalGrid}>
                      <Grid item lg={12} md={12} sm={12} xs={12} >
                        <TextField 
                          key={value._id+index}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="ddd"
                          onChange={(event) => handleChangeValue(index, 'ddd', event.target.value)}
                          value={value.ddd}
                          error={handleArrayInputError("contact", "error", index, "ddd")}                          
                          helperText={handleArrayInputError("contact", "text", index, "ddd")}   
                        />
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="telefone"
                          onChange={(event) => handleChangeValue(index, 'tel', event.target.value)}
                          value={value.telefone}
                          error={handleArrayInputError("contact", "error", index, "telefone")}                          
                          helperText={handleArrayInputError("contact", "text", index, "telefone")} 
                        />
                      </Grid> 
                    </Grid>
                  )
                )}
                <br/>
                <Button
                  variant="text"
                  color="primary"
                  className={classes.button}
                  startIcon={<AddIcon/>}
                  onClick={() => handleAddElement('contato')}
                >
                  Adicionar Contato
                </Button>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancelar
              </Button>
              <Button onClick={() => handleSubmitUpdate("contato")} color="primary">
                Salvar
              </Button>
            </DialogActions>
          </Dialog>
        </div>

        <div>
          <Dialog
            open={openEndereco}
            onClose={handleClose}
            scroll={scroll}
          >
            <DialogTitle id="scroll-dialog-title">Alteração de Endereço Cadastrado</DialogTitle>
            <DialogContent dividers={scroll === 'paper'}>
              <DialogContentText
                id="scroll-dialog-description"
                ref={descriptionElementRef}
                tabIndex={-1}
              >
                
                {
                  enderecos.map( (value, index) => (
                    <Grid className={classes.modalGrid}>
                      <Grid item lg={12} md={12} sm={12} xs={12} >
                        <Grid>
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="CEP"
                          onChange={(event) => handleChangeValue(index, 'cep', event.target.value)}
                          value={value.cep}
                          error={handleArrayInputError("endereco", "error", index, "cep")}                          
                          helperText={handleArrayInputError("endereco", "text", index, "cep")}
                        />
                        </Grid>
                        <Grid>
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Rua"
                          onChange={(event) => handleChangeValue(index, 'rua', event.target.value)}
                          value={value.rua}
                          error={handleArrayInputError("endereco", "error", index, "rua")}                          
                          helperText={handleArrayInputError("endereco", "text", index, "rua")}
                        />
                        </Grid>
                        <Grid>
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Número"
                          onChange={(event) => handleChangeValue(index, 'num', event.target.value)}
                          value={value.numero}
                          error={handleArrayInputError("endereco", "error", index, "num")}                          
                          helperText={handleArrayInputError("endereco", "text", index, "num")}
                        />
                        </Grid>
                        <Grid>
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Cidade"
                          onChange={(event) => handleChangeValue(index, 'city', event.target.value)}
                          value={value.cidade}
                          error={handleArrayInputError("endereco", "error", index, "city")}                          
                          helperText={handleArrayInputError("endereco", "text", index, "city")}
                        />
                        </Grid>
                        <Grid>
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Latitude"
                          onChange={(event) => handleChangeValue(index, 'lat', event.target.value)}
                          value={value.lat}
                          error={handleArrayInputError("endereco", "error", index, "lat")}                          
                          helperText={handleArrayInputError("endereco", "text", index, "lat")}
                        />
                        </Grid>
                        <Grid>
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Longitude"
                          onChange={(event) => handleChangeValue(index, 'long', event.target.value)}
                          value={value.long}
                          error={handleArrayInputError("endereco", "error", index, "long")}                          
                          helperText={handleArrayInputError("endereco", "text", index, "long")}
                        />
                        </Grid>
                      </Grid>
                    </Grid>
                  )
                )}
                <br/>
                <Button
                  variant="text"
                  color="primary"
                  className={classes.button}
                  startIcon={<AddIcon/>}
                  onClick={() => handleAddElement('endereco')}
                >
                  Adicionar Endereço
                </Button>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancelar
              </Button>
              <Button onClick={() => handleSubmitUpdate("endereco")} color="primary">
                Salvar
              </Button>
            </DialogActions>
          </Dialog>
        </div>

        <div>
          <Dialog
            open={openHorario}
            onClose={handleClose}
            scroll={scroll}
          >
            <DialogTitle id="scroll-dialog-title">Alteração de Horário Cadastrado</DialogTitle>
            <DialogContent dividers={scroll === 'paper'}>
              <DialogContentText
                id="scroll-dialog-description"
                ref={descriptionElementRef}
                tabIndex={-1}
              >
                
                {
                  horarios.map( (value, index) => (
                    <Grid className={classes.modalGrid}>
                      <Grid item lg={12} md={12} sm={12} xs={12} >
                        <Grid>
                        <TextField 
                          key={value._id}
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Dia da Semana"
                          onChange={(event) => handleChangeValue(index, 'dia', event.target.value)}
                          value={value.date}
                          error={handleArrayInputError("horario", "error", index, "dia")}                          
                          helperText={handleArrayInputError("horario", "text", index, "dia")}
                        />
                        </Grid>
                        <Grid>
                        <TextField 
                          key={value._id}
                          type="time"
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Abertura"
                          onChange={(event) => handleChangeValue(index, 'inicio', event.target.value)}
                          value={value.horario_inicio ? value.horario_inicio : '00:00'}
                          error={handleArrayInputError("horario", "error", index, "inicio")}                          
                          helperText={handleArrayInputError("horario", "text", index, "inicio")}
                        />
                        </Grid>
                        <Grid>
                        <TextField 
                          key={value._id}
                          type="time"
                          sx={{ m: 1 }}
                          variant="outlined" 
                          label="Fechamento"
                          onChange={(event) => handleChangeValue(index, 'fim', event.target.value)}
                          value={value.horario_fim ? value.horario_fim : '00:00'}
                          error={handleArrayInputError("horario", "error", index, "fim")}                          
                          helperText={handleArrayInputError("horario", "text", index, "fim")}
                        />
                        </Grid>
                      </Grid> 
                    </Grid>
                  )
                )}
                <br/>
                <Button
                  variant="text"
                  color="primary"
                  className={classes.button}
                  startIcon={<AddIcon/>}
                  onClick={() => handleAddElement('horario')}
                >
                  Adicionar Horário
                </Button>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancelar
              </Button>
              <Button onClick={() => handleSubmitUpdate("horario")} color="primary">
                Salvar
              </Button>
            </DialogActions>
          </Dialog>
        </div>

        <Snackbar open={success} autoHideDuration={6000} onClose={handleCloseSucces}>
          <Alert icon={false}  onClose={handleCloseSucces} severity="success">
            {successText}
          </Alert>
        </Snackbar>
      
        <Snackbar open={err} autoHideDuration={6000} onClose={handleCloseErr}>
          <Alert icon={false} onClose={handleCloseErr} severity="error">
            {errText}
          </Alert>
        </Snackbar>

    </Grid> 
    
  )
}

export default ActivateAccount
