import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { isInstituicao, IsLogged, Logout, myAccount } from '../../../api/AccountController'

//MUI COMPONENTS
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import { makeStyles } from '@material-ui/core'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';


//STYLES
import styles from './style'

//COMPONENTS
import GridHeader from '../../../Components/GridHeader'
import RoundDiv from '../../../Components/RoundDiv';

//ICONS
import CreateIcon from '@material-ui/icons/Create'
import VisibilityIcon from '@material-ui/icons/Visibility'
import PersonIcon from '@material-ui/icons/Person'
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles({
    gridHeader: {
        borderEndStartRadius: '35%',
        borderEndEndRadius: '35%',
        height: '22vh',
        boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
        backgroundImage: 'linear-gradient(#FF9900, #FDC97A)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    marginTop: {
        marginTop: '5%',
        justifyContent: 'left',
        alignItems: 'left',
    },
    marginItems: {
        marginTop: '5%',
        marginBottom: '5%',
    },    
})

function ActivateAccount() {
  const classes = useStyles()
  const history = useHistory();

  const [name, setName] = useState("")
  const [city, setCity] = useState("")
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    document.title = 'Home Instituição';

    if(!IsLogged())
      history.push('/login')

    if(!isInstituicao())
      history.push('/login')

    setLoading(true)
    myAccount().then(value =>{
      if(value){
        setName(value.data.userData.nome)
        setCity(value.data.userData.addresses[0].cidade)
      }
    })
    setLoading(false)
      
    
  });

  function handleLogout (){
    Logout(history);
  }

  function handleInstitution(necessity) {
    history.push(`/${necessity}`)
  }

  return (
    <Grid>
      {
        !loading &&
          <GridHeader 
              name={name}
              city={city}
              back={false}
          />
      }

        <Grid className={classes.marginTop}>

        <div className={classes.root}>
          <List component="nav">
            <ListItem
              button
              onClick={() => handleInstitution('newNecessity')}
              /* selected={selectedIndex === 0}
              onClick={(event) => handleListItemClick(event, 0)} */
            >
              <ListItemIcon>
                <CreateIcon />
              </ListItemIcon>
              <ListItemText primary="Cadastrar Nova Necessidade" />
            </ListItem>
          </List>
          <Divider />
          <List component="nav">
            <ListItem
              button
              onClick={() => handleInstitution('showNecessity')}
              /* selected={selectedIndex === 0}
              onClick={(event) => handleListItemClick(event, 0)} */
            >
              <ListItemIcon>
                <VisibilityIcon />
              </ListItemIcon>
              <ListItemText primary="Visualizar Necessidades" />
            </ListItem>
          </List>  
          <Divider />
          <List component="nav">
            <ListItem
              button
              onClick={() => handleInstitution('myAccount')}
              /* selected={selectedIndex === 0}
              onClick={(event) => handleListItemClick(event, 0)} */
            >
              <ListItemIcon>
                <PersonIcon />
              </ListItemIcon>
              <ListItemText primary="Dados Cadastrados" />
            </ListItem>
          </List> 
          <Divider />
          <List component="nav">
            <ListItem
              button
              onClick={handleLogout}
              /* selected={selectedIndex === 0}
              onClick={(event) => handleListItemClick(event, 0)} */
            >
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              <ListItemText primary="Fazer LogOut" />
            </ListItem>
          </List>      
        </div>
          
            
            
        </Grid>

    </Grid> 
  )
}

export default ActivateAccount
