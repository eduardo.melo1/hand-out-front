import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { isInstituicao, IsLogged, Logout, myAccount } from '../../../api/AccountController'
import { ShowNecessity, Enablenecessity, Disablenecessity } from '../../../api/InstitutionController'

//MUI COMPONENTS
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import Button from '@mui/material/Button'
import { makeStyles } from '@material-ui/core'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';


//COMPONENTS
import GridHeader from '../../../Components/GridHeader'
import FiltroRow from '../../../Components/Filtro/FiltroRow';

//ICONS
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'


const useStyles = makeStyles({
    gridHeader: {
        borderEndStartRadius: '35%',
        borderEndEndRadius: '35%',
        height: '22vh',
        boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
        backgroundImage: 'linear-gradient(#FF9900, #FDC97A)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    marginTop: {
        marginTop: '5%',
        justifyContent: 'left',
        alignItems: 'left',
        width: '100%'
    },
    title: {
      textAlign: 'center'
    },
    marginItems: {
        marginTop: '5%',
        marginBottom: '5%',
        
    },
    necessity: {
      marginTop: '10%',
      width:'80%',
      alignItems: 'center',
      justifyContent: 'space-between',
      alignSelf:'center',
    },
    necessity_text: {
      padding: '10%'
    },
    itens: {
      backgroundColor: '#D7D7D7'
    }
})

function ActivateAccount() {
  const classes = useStyles()
  const history = useHistory();

  const [id, setId] = useState("")
  const [name, setName] = useState("")
  const [city, setCity] = useState("")
  const [necessities, setNecessities] = useState([])
  const [loading, setLoading] = useState(false)

  const [ativo, setAtivo] = useState(false)
  const [inativo, setInativo] = useState(false)

  useEffect(() => {
    document.title = 'Necessidades Cadastradas';

    if(!IsLogged())
      history.push('/login')

    if(!isInstituicao())
      history.push('/login')

    setLoading(true)
    myAccount().then(value =>{
      if(value){
        setId(value.data.userData._id)
        setName(value.data.userData.nome)
        setCity(value.data.userData.addresses[0].cidade)
      }
    })
    setLoading(false)

    setLoading(true)
    ShowNecessity().then(value => {
      setNecessities(value)
    })
    setLoading(false)
  });

  function handleLogout (){
    Logout(history);
  }

  const handleChangeStatusNecessity = (e) => {
    if(e.isActive) {
      Disablenecessity(id, e._id).then(value => {
        history.push('/showNecessity')
      })
    }
    else {
      Enablenecessity(id, e._id).then(value => {
        history.push('/showNecessity')
      })
    }
  }

  return (
    <Grid>
         {
            !loading &&
              <GridHeader 
                  name={name}
                  city={city}
                  back={true}
              />
          }

        <Grid className={classes.marginTop}>
          
            <Grid item lg={12} md={12} sm={12} xs={12} className={classes.title}>
                <Typography 
                    variant="h5" 
                    color="text.black"
                >
                    Necessidades Cadastradas
                </Typography>
            </Grid>

            <Grid item xs={12} md={6}>

          <div className={classes.necessity}>
            <List>
              {
                necessities.map((e,index) => (
                  <ListItem>
                    <ListItemText
                      primary={e.nome}
                      secondary={e.id_categoria.tipo}
                    />
                    <ListItemSecondaryAction>
                      <IconButton 
                        edge="end"
                        onClick={() => handleChangeStatusNecessity(e)}
                      >
                        {
                          e.isActive ? 
                            <VisibilityIcon /> 
                          : 
                            <VisibilityOffIcon />
                        }
                        
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))

              }
            </List>
          </div>

        </Grid>
            
        </Grid>

    </Grid> 
  )
}

export default ActivateAccount
