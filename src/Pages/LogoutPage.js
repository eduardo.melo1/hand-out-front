import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Logout } from '../api/AccountController';

import '../Styles/EntryPages.css'

function LogoutPage() {
    const history = useHistory();

    useEffect(() => {
        document.title = 'Logout';

        handleLogout();
    });

    function handleLogout (){
        console.log('a')
        Logout(history);
    }

    return (
        <div class="page">
        </div>
    )
}

export default LogoutPage
