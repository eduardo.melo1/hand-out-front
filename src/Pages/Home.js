import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { isInstituicao, IsLogged, Logout } from '../api/AccountController';
import InNatura from '../Components/Categorias/InNatura';
import MinimamenteProcessados from '../Components/Categorias/MinimamenteProcessados';
import Processados from '../Components/Categorias/Processados';
import Ultraprocessados from '../Components/Categorias/Ultraprocessados';

import '../Styles/EntryPages.css'

function ActivateAccount() {
    const history = useHistory();

    useEffect(() => {
        document.title = 'Home';

        if(!IsLogged())
            history.push('/login');
        if(!isInstituicao()){
            history.push('/homeInstitution');
        }else{
            history.push('/map');
        }
        
        
    });

    function handleLogout (){
        Logout(history);
    }


    //O operador ternario abaixo esta com os valores invertidos e não tenho a minima ideia do porque HELP
    return (
        <div class="page">
            <h1>Você está logado como: {isInstituicao() ? 'Doador' :'Instituição' }</h1>

            <button onClick={handleLogout}>Logout</button>
            
        </div>
    )
}

export default ActivateAccount
