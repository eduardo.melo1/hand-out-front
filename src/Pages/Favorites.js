import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { getUserId, isInstituicao, IsLogged, Logout, myAccount } from '../api/AccountController';
import CategoriaRow from '../Components/Categorias/CategoriaRow';
import GridHeader from '../Components/GridHeader';
import { useParams } from "react-router-dom";
import { getFavorites, institutionData } from '../api/PageInfoController';

import '../Styles/InstitutionDetails.css'
import CategoriaGroup from '../Components/Categorias/CategoriaGroup';
import ContactBox from '../Components/ContactBox';
import ReturnArrow from '../Components/ReturnButton';
import FavoriteInstitutionButton from '../Components/FavoriteInstitutionButton';
import InstitutionBox from '../Components/InstitutionBox';

function Favorites() {
    const history = useHistory();

    const [userData, setUserData] = React.useState({nome: ""});
    const [favorites, setFavorites] = React.useState();

    useEffect(() => {

        document.title = 'Hand Out';

        if(!IsLogged())
            history.push('/login');

        updateUserData();
        updateFavorites();
        return () => {
            setUserData({})
        };
    }, []);

    async function updateUserData() {
        const response = await myAccount();

        if(!response || response.status >= 400){
            console.log(response);
        }else {
            setUserData(response.data.userData);
        }
    }

    async function updateFavorites() {
        const response = await getFavorites(getUserId());

        if(!response || response.status >= 400){
            console.log(response);
            history.push('/')
        }else {
            if(!response.favorites)
                history.push('/');
                populateFavorites(response.favorites);

        }        
    }

    async function populateFavorites(favorites) {
        let favoriteList = [];

        if(favorites){
            for (const index in favorites) {
                const favorite = favorites[index];
                const institution = await getInstitutionData(favorite.id_fav);
                favoriteList.push(
                    institution
                )
            }
        }
        setFavorites(favoriteList)
    }

    async function getInstitutionData(id_inst) {
        const response = await institutionData({id: id_inst});
        // const favorites = await getFavorites();

        // console.log(favorites)

        if(!response || response.status >= 400){
            console.log(response);
            history.push('/')
        }else {
            if(!response.data.institution)
                history.push('/');
            return response.data.institution
        }
             
    }
    
    function renderFavorites() {
        let favoriteList = [];

        if(favorites){
            for (const index in favorites) {
                const favorite = favorites[index];
                const necessities = getNecessities(favorite)
                favoriteList.push(
                    <InstitutionBox reload={true} name={favorite.inst[0].nome} institutionId={favorite.inst[0]._id} {...necessities}/>
                )
            }
        }
        return favoriteList;
    }

    function getNecessities(institution){
        let minimamente=false;
        let ultra=false;
        let inNatura=false;
        let processados=false;

        if(institution.necess)
            for (const index in institution.necess) {
                if(institution.necess[index].isActive){
                    switch (institution.necess[index].id_categoria.tipo) {
                        case 'Minimamente Processado':
                            minimamente=true;
                            break;

                        case 'Ultraprocessado':
                            ultra=true;
                            break;
                    
                        case 'In Natura':
                            inNatura=true;
                            break;


                        case 'Processado':
                            processados=true;
                            break;

                        default:
                            break;
                    }
                }
            }

        return({minimamente, ultra, inNatura, processados})
    }

    //O operador ternario abaixo esta com os valores invertidos e não tenho a minima ideia do porque HELP
    return (
        <div class="page">
            <ReturnArrow/>

            <GridHeader 
                name={userData.nome}
                back={false}
            />

            <div className="content">
                <div className="favorites">
                    {renderFavorites()}
                </div>
            </div>

        </div>
    )
}

export default Favorites
