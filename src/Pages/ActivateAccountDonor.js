import React, { useEffect, useState } from 'react';
import ValidationFormsContainer from '../Components/ValidationFormsContainer';
import LoginForm from '../Components/LoginForm';
import { useHistory } from 'react-router-dom';
import { IsLogged, VerifyAccountDonor } from '../api/AccountController';

import '../Styles/EntryPages.css'
import Modal from '../Components/Modal';

function ActivateAccountDonor() {
    const history = useHistory();

    const [title, setTitle] = useState('Sua conta foi ativada com sucesso');
    const [text, setText] = useState('Agora você já pode realizar seu login em nosso sistema!');
    const [btnText, setBtnText] = useState('Realizar Login');

    useEffect(() => {
        document.title = 'Confirmação de conta';

        if(IsLogged())
            history.push('/');

        VerifyAccountDonor(window.location.pathname).then(value => {
            if(value.data && value.data.err){
                console.log(value)
                setTitle('Ops, um erro aconteceu!');
                setText('Parece que o link que você está usando é invalido ou expirou, se o erro persistir contate os administradores');
                setBtnText('Voltar ao login');
            }
        })
    });

    const [open, setOpen] = React.useState(true);

    const handleClose = () => {
        setOpen(false);
        history.push('/login');
    };

    return (
        <div class="page">
            <ValidationFormsContainer></ValidationFormsContainer>
            <LoginForm></LoginForm>
            <Modal 
                title={title} 
                text={text}
                btnText={btnText}
                open={open}
                handleClose={handleClose}>
            </Modal>
        </div>
    )
}

export default ActivateAccountDonor
