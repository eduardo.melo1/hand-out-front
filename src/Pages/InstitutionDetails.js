import React, { useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { isInstituicao, IsLogged, Logout, myAccount } from '../api/AccountController';
import CategoriaRow from '../Components/Categorias/CategoriaRow';
import GridHeader from '../Components/GridHeader';
import { useParams } from "react-router-dom";
import { getFavorites, institutionData } from '../api/PageInfoController';

import '../Styles/InstitutionDetails.css'
import CategoriaGroup from '../Components/Categorias/CategoriaGroup';
import ContactBox from '../Components/ContactBox';
import ReturnArrow from '../Components/ReturnButton';
import FavoriteInstitutionButton from '../Components/FavoriteInstitutionButton';

function InstitutionDetails() {
    const history = useHistory();
    const { id_inst } = useParams();

    const [institucionData, setInstitucionData] = React.useState({});
    const [institutionNecessities, setinstitutionNecessities] = React.useState();

    useEffect(() => {

        document.title = 'Detalhes Instituição';

        if(!IsLogged())
            history.push('/login');

        updateInstitutionData()
    }, [])

    function updateNecessities(institution){
        let minimamente=[];
        let ultra=[];
        let inNatura=[];
        let processados=[];

        if(institution.necess)
            for (const index in institution.necess) {
                if(institution.necess[index].isActive){
                    switch (institution.necess[index].id_categoria.tipo) {
                        case 'Minimamente Processado':
                            minimamente.push(<li>{institution.necess[index].nome}</li>);
                            break;

                        case 'Ultraprocessado':
                            ultra.push(<li>{institution.necess[index].nome}</li>);
                            break;
                    
                        case 'In Natura':
                            inNatura.push(<li>{institution.necess[index].nome}</li>);
                            break;


                        case 'Processado':
                            processados.push(<li>{institution.necess[index].nome}</li>);
                            break;

                        default:
                            break;
                    }
                }
            }

        setinstitutionNecessities({minimamente, ultra, inNatura, processados})
    }

    async function updateInstitutionData() {
        const response = await institutionData({id: id_inst});

        if(!response || response.status >= 400){
            console.log(response);
            history.push('/')
        }else {
            if(!response.data.institution)
                history.push('/');
            setInstitucionData(response.data.institution);
            updateNecessities(response.data.institution);
        }        
    }

    function renderContacts() {
        let contatosList = [];

        if(institucionData.inst)
            for (const index in institucionData.inst[0].contacts) {
                const contato = institucionData.inst[0].contacts[index];
                const contatoFormated = '(' + contato.ddd + ') ' + contato.telefone
                contatosList.push(<ContactBox text={contatoFormated} />)
            }

        return contatosList;
    }

    //O operador ternario abaixo esta com os valores invertidos e não tenho a minima ideia do porque HELP
    return (
        <div class="page">
            <ReturnArrow/>
            <div>
                <FavoriteInstitutionButton name={institucionData.inst ? institucionData.inst[0].nome : "esta instituição"} instId={institucionData.inst ? institucionData.inst[0]._id : ''}/>
            </div>
            <GridHeader 
                name={institucionData.inst ? institucionData.inst[0].nome : "undefined"}
                back={false}
                city={institucionData.inst ? institucionData.inst[0].addresses[0].cidade : "undefined"}
            />
            <div className="institutionDetailsContent">
                <div className="description">
                    <p>
                        {institucionData.inst ? institucionData.inst[0].descricao : ''}
                    </p>     
                </div>

                <div clasname="contacts">
                    {renderContacts()}
                </div>
                <p className="description">Aceitando doações de:</p>
                <CategoriaGroup {...institutionNecessities}></CategoriaGroup>
            </div>

        </div>
    )
}

export default InstitutionDetails
