import React, { useEffect, useState } from 'react'

import { changePWD, IsLogged, myAccount } from '../api/AccountController';
import { useHistory } from 'react-router';
import { Button, Container, FormControl, Grid, IconButton, InputAdornment, InputLabel, makeStyles, OutlinedInput, Snackbar, TextField } from '@material-ui/core'
import GridHeader from '../Components/GridHeader';
import ReturnArrow from '../Components/ReturnButton';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Alert } from '@mui/material';


const useStyles = makeStyles({
    defaultGrid: {
      marginTop: '10%',
      margin: 'auto',
      width:'80%',
      display: 'flex',
      flexDirection: 'row',
      alignContent: 'center',
      alignItems: 'center',
      textAlign:'center',
    },
    center: {
        marginTop: '10vh',
        display: 'flex',
        flexDirection: 'column'

    },
    inputField: {
        "& > *": {
            boxShadow: 'none',
        }
    },
    disabledInput: {
        width: "80vw",
    },
    buttonsSpace: {
        display: 'flex !important',
        justifyContent: 'space-around !important',
    },
    confirmButton: {
        border: '1px',
        boxShadow: '0 4px 4px 0 #404040',
    },
    orange: {
        backgroundColor: '#FF9900 !important',
    },    
    white: {
        backgroundColor: '#FDF9F9',
    }
    
  })

export default function MyAccountUser() {
    const classes = useStyles();
    const history = useHistory();
    const size = '22.5vw';

    const [userData, setUserData] = React.useState({nome: ""});
    const [open, setOpen] = React.useState(false);
    const [openSnack, setOpenSnack] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
      });

    useEffect(() => {

        document.title = 'Hand Out';

        if(!IsLogged())
            history.push('/login');

        updateUserData();
        return () => {
            setUserData({})
        };
    }, []);

    async function updateUserData() {
        const response = await myAccount();

        if(!response || response.status >= 400){
            console.log(response);
        }else {
            setUserData(response.data.userData);
        }
    };

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
      };
    
    const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
      };
    
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };

    const handleClickOpen = () => {
      setValues({
        ...values,
        password: '',
      });
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const handleSubmit = async () => {
        if(!isPasswordInvalid()){
            handleClose()
            const response = await changePWD({email:userData.email ,senha: values.password});

            if(!response || response.status >= 400){
                console.log(response);
                setSuccess(false)
                // history.push('/')
            }else {
                setSuccess(true)
                console.log(response)
            }
            handleClickSnack();
        }
    }
    const isPasswordInvalid = () => {
        return (values.password.length < 8 || values.password.length > 12)
    };

    const handleClickSnack = () => {
        setOpenSnack(true);
    };
    
    const handleCloseSnack = (event, reason) => {
        setOpenSnack(false);
    }
  
    
    return (
        <div>
            <ReturnArrow/>
            <GridHeader 
                name={userData.nome}
                back={false}
            />

            <Grid item lg={12} md={12} sm={12} xs={12}>
            <Container className={classes.defaultGrid}>
            <TextField 
                sx={{ m: 1 }}
                className={classes.disabledInput}
                variant="outlined" 
                value={userData.email}
                disabled
                error={false}
            />
            </Container>
            <br/>
            <div className={classes.center}>
                <input className="submit" type="submit" value="Alterar Senha" onClick={handleClickOpen} />
                <br/>
                <input className="submit" type="submit" value="Fazer Logout" onClick={() => history.push('/logout')} />
            </div>
            </Grid>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Alterar Senha</DialogTitle>
                <DialogContent>
                <DialogContentText>
                    Digite uma nova senha para sua conta
                </DialogContentText>
                <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-password">
                    </InputLabel>
                    <TextField className={classes.inputField}
                        error={isPasswordInvalid()}
                        id="outlined-adornment-password"
                        type={values.showPassword ? "text" : "password"}
                        value={values.password}
                        onChange={handleChange("password")}
                        helperText={ isPasswordInvalid() ? 'A senha deve Possuir pelo menos 6 caracteres e no máximo 12' : false}
                        variant="outlined"
                        endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                            >
                            {values.showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                        </InputAdornment>
                        }
                        label="Senha"
                    />
                </FormControl>
                </DialogContent>
                <DialogActions className={classes.buttonsSpace}>
                    <Button className={`${classes.confirmButton} ${classes.orange}`} onClick={handleClose}>Cancelar</Button>
                    <Button className={`${classes.confirmButton} ${classes.white}`} onClick={handleSubmit}>Confirmar</Button>
                </DialogActions>
            </Dialog>

            <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnack}>
                <Alert onClose={handleCloseSnack} severity={success ? "success" : "error"} sx={{ width: '100%' }}>
                    {success ? "Senha Atualizada com sucesso" : "Ocorreu um erro ao atualizar a senha, tente novamente mais tarde"}
                </Alert>
            </Snackbar>
        </div>
    )
}