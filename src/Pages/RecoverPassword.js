import React, { useEffect } from 'react'
import ValidationFormsContainer from '../Components/ValidationFormsContainer';
import { IsLogged, requestPwdRecovery } from '../api/AccountController';

import '../Styles/EntryPages.css'
import { useHistory } from 'react-router';
import Modal from '../Components/Modal';
import RecoverPwdForm from '../Components/RecoverPwdForm';

function RecoverPassword() {
    const history = useHistory();

    useEffect(() => {
        document.title = 'Recuperar Senha';

        if(IsLogged())
            history.push('/');
    });

    //dialog setings
    const title = 'Recuperação de senha concluida';
    const text = 'Se o email inserido estiver cadastrado no nosso sistema um email foi enviado para você';
    const btnText = 'Fechar';

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        history.push('/login');
    };

    const RecoverPwdApiCall = async details => {
        const response = await requestPwdRecovery({...details});

        handleClickOpen();
        
    }

    return (
        <div class="page">
            <ValidationFormsContainer></ValidationFormsContainer>
            <RecoverPwdForm requestRecovery={RecoverPwdApiCall}></RecoverPwdForm>
            <Modal 
                title={title} 
                text={text}
                btnText={btnText}
                open={open}
                handleClose={handleClose}>
            </Modal>
        </div>
    )
    
}

export default RecoverPassword
