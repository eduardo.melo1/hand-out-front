import React, { useEffect } from 'react'
import ValidationFormsContainer from '../Components/ValidationFormsContainer';
import RegisterForm from '../Components/RegisterForm';
import Modal from '../Components/Modal';
import { IsLogged, RegisterAcc } from '../api/AccountController';

import '../Styles/EntryPages.css'
import { useHistory } from 'react-router';

function Register() {

    const history = useHistory();

    useEffect(() => {
        if(IsLogged())
            history.push('/');

        document.title = 'Cadastrar';
    });
    
    //dialog setings
    const title = 'Seu Cadastro foi realizado com sucesso!';
    const text = 'Acesse o e-mail cadastrado para ativar sua conta e poder realizar seu login no sistema!';
    const btnText = 'Fechar';

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        history.push('/login');
    };


    const Register = async details => {
        const data = await RegisterAcc({...details});

        if(!(data.status && data.status >= 400))
            handleClickOpen();

        return data;
    }

    return (
        <div class="page">
            <ValidationFormsContainer></ValidationFormsContainer>
            <RegisterForm Register={Register}></RegisterForm>
            <Modal 
                title={title} 
                text={text}
                btnText={btnText}
                open={open}
                handleClose={handleClose}>
            </Modal>
        </div>
    )
}

export default Register
