import React, { useEffect, useState } from 'react'
import L from 'leaflet'
import { MapContainer, TileLayer, Marker, Popup, useMap } from 'react-leaflet'

import '../../Styles/Map.css';
import SideNav from '../../Components/Sidenav';
import { IsLogged, myAccount } from '../../api/AccountController';
import { useHistory } from 'react-router';
import { makeStyles } from '@material-ui/core'
import GridHeader from '../../Components/GridHeader';
import RoundDiv from '../../Components/RoundDiv';
import { getInstitutions } from '../../api/PageInfoController';

import '../../Styles/CategoriasRow.css'
import Ultraprocessados from '../../Components/Categorias/Ultraprocessados'
import Processados from '../../Components/Categorias/Processados'
import MinimamenteProcessados from '../../Components/Categorias/MinimamenteProcessados'
import InNatura from '../../Components/Categorias/InNatura'


function Map() {
    const history = useHistory();
    const size = '22.5vw';

    const [firstExecution, setFirstExecution] = React.useState(true)
    const [position, setPosition] = useState(null);
    const [userData, setUserData] = React.useState({nome: ""});
    const [institutions, setInstitutions] = React.useState();
    const [categories, setCategories] = React.useState({minimamente: false, ultra:false , inNatura: false, processados: false});

    useEffect(() => {

        document.title = 'Hand Out';

        if(!IsLogged())
            history.push('/login');

        updateUserData();
        updateInstitutionsData();
        return () => {
            setUserData({})
        };
    }, []);

    function GetIcon(){
        return L.icon({
            iconUrl: require('../../assets/location.png'),
            iconSize: 40
        });
    }

    async function updateUserData() {
        const response = await myAccount();

        if(!response || response.status >= 400){
            console.log(response);
        }else {
            setUserData(response.data.userData);
        }
    }
    

    async function updateInstitutionsData() {
        const response = await getInstitutions();

        if(!response || response.status >= 400){
            console.log(response);
        }else {
            // const filteredInstitution = filterInstitutions(response.data.institutions)
            setInstitutions(response.data.institutions);
        }
    }

    function renderInstitutions() {
        let institutionList = [];

        if(institutions) {
            for (const institutionIndex in institutions) {
                const institution = institutions[institutionIndex];

                if(shouldShowInstitution(institution)){
                    for (const addressIndex in institution.addresses) {
                        const address = institution.addresses[addressIndex];   
                            institutionList.push(
                                <Marker position={[+address.lat, +address.long]}>
                                    <Popup className="institutionPopup">
                                        <h2>{institution.nome}</h2>
                                        <button onClick={() => openInstitution(institution._id)}>Ver Detalhes</button>
                                    </Popup>
                                </Marker>
                        )
                    }
                }
            }
        }
        return institutionList;
    }

    function shouldShowInstitution(institution){
        if(institution){
            for (const categoriaIndex in institution.categorias) {
                if (Object.hasOwnProperty.call(institution.categorias, categoriaIndex)) {
                    const categoria = institution.categorias[categoriaIndex].tipo;
                    switch (categoria) {
                        case 'Minimamente Processado':
                            if(categories.minimamente)
                                return true;
                            break;
            
                        case 'Ultraprocessado':
                            if(categories.ultra)
                                return true;
                            break;
            
                        case 'In Natura':
                            if(categories.inNatura)
                                return true;
                            break;
            
                        case 'Processado':
                            if(categories.processados)
                                return true;
                            break;
            
                        default:
                            break;
                    }
    
                }
            }
        }
        return !(categories.inNatura || categories.minimamente || categories.processados || categories.ultra);
    }

    function openInstitution(id) {
        history.push(`/show-institution/${id}`);
    }

    function toggleFilter(categoria){
        let minimamente=!!categories.minimamente;
        let ultra=!!categories.ultra;
        let inNatura=!!categories.inNatura;
        let processados=!!categories.processados;

        switch (categoria) {
            case 'Minimamente Processado':
                minimamente=!minimamente;
                break;

            case 'Ultraprocessado':
                ultra=!ultra;
                break;

            case 'In Natura':
                inNatura=!inNatura;
                break;

            case 'Processado':
                processados=!processados;
                break;

            default:
                break;
        }

        setCategories({minimamente, ultra, inNatura, processados})
    }

    
    function MoveToLocation() {
        const map = useMap();

        useEffect(() => {
            if(firstExecution){
                map.locate().on("locationfound", function(e) {
                    setFirstExecution(false);
                    setPosition(e.latlng);
                    map.flyTo(e.latlng, map.getZoom());
                });
            }
        }, []);

        return position === null ? null : (
            <Marker position={position}>
                <Popup>Você está Aqui</Popup>
            </Marker>
        );
    };

    return (
        <div>
            <GridHeader 
                name={userData.nome}
                back={false}
                overlap={true}
            />
            <SideNav/>
            <MapContainer 
                center={[-15.7975, -47.8919]}
                zoom={15}
                zoomControl={false}
                >
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <MoveToLocation />
            <div clasname="markers">
                {renderInstitutions()}
            </div>

            <RoundDiv>
                aaaa
            </RoundDiv>
            </MapContainer>
            <div className="filterRow">
                <div onClick={() => toggleFilter("Ultraprocessado")}>
                    <Ultraprocessados disabled={!categories.ultra} size={size}></Ultraprocessados>
                </div>
                <div onClick={() => toggleFilter("Processado")}>
                    <Processados disabled={!categories.processados} size={size}></Processados>
                </div>
                <div onClick={() => toggleFilter("Minimamente Processado")}>
                    <MinimamenteProcessados disabled={!categories.minimamente} size={size}></MinimamenteProcessados>
                </div>
                <div onClick={() => toggleFilter("In Natura")}>
                    <InNatura disabled={!categories.inNatura} size={size}></InNatura>
                </div>
            </div>
        </div>
    )
}

export default Map
