import { Height } from '@material-ui/icons'
import { width } from '@mui/system'
import React from 'react'
import RoundDiv from '../RoundDiv'
import '../../Styles/RoundDiv.css'

//MATERIAL COMPONENTS
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'

//ICONS
import VisibilityIcon from '@material-ui/icons/Visibility'

export default function Ativo(props) {
    return (
        <Button
            onClick={props.onClick}
        >
            <RoundDiv size={props.size || '100px'}>
                <VisibilityIcon/>
                <Typography 
                    variant="body1" 
                    color="text.black"
                >
                    Ativas
                </Typography>
            </RoundDiv>
        </Button>
        
    )
}