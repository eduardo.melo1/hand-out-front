import React from 'react'
import '../../Styles/CategoriasRow.css'
import Ativo from './Ativo'
import Inativo from './Inativo'

export default function CategoriaRow(props) {
    const size = '22.5vw';
 
    return (
        <div className="filterRow">
            <Ativo size={size} onClick={props.ativas}/>
            <Inativo size={size} onClick={props.inativas} />
        </div>
    )
}