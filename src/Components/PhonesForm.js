import React from 'react'
import { useForm } from "react-hook-form"
import '../Styles/FormStyles.css'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import ReactInputMask from 'react-input-mask';


export default function PhonesForm({ ReturnPhones }) {

    const { register, handleSubmit, formState: { errors }} = useForm();
    const onSubmit = data => {
        ReturnPhones(data).then(value =>{
        })
    };

    return (
        <div class="form-inner">

            <div className="form-group">
                <ReactInputMask mask="(99) 99999-9999" type="text" placeholder="Telefone" {...register('telefone', { required: true, minLength: 10, maxLength: 12})} disableUnderline/>
                <input name="phone" type="text" placeholder="Telefone" {...register('telefone', { required: true, minLength: 10, maxLength: 12})} />
                <div class="errors">
                </div> 

            </div>

            <div className="form-group">
                <input className="submit" type="submit" value="Fazer Login" onClick={handleSubmit(onSubmit)} />
                <a class="center recover-pw-anchor" href="recover-password">Esqueceu a senha?</a>
            </div>


            <h4 class="center footer-message">Ainda não tem uma conta?<a class="register-anchor" href="register">Cadastre-se</a></h4>
        </div>
    )
}