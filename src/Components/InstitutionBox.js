import { Button } from '@material-ui/core';
import React from 'react'
import '../Styles/InstitutionBox.css'

import InNaturaLogo from '../assets/InNatura.png'
import MinimamenteProcessadosLogo from '../assets/MinimamenteProcessados.png'
import ProcessadosLogo from '../assets/Processados.png'
import UltraprocessadosLogo from '../assets/Ultraprocessados.png'
import { useHistory } from 'react-router';
import FavoriteInstitutionButton from './FavoriteInstitutionButton';

const favoriteButtonStyle = {
    boxShadow: 'none',
    backgroundColor: 'transparent',
};

export default function InstitutionBox(props) {
    const history = useHistory();

    const [isFavorite, setIsFavorite] = React.useState(false);

    const size = '22.5vw';

    function openInstitution(id) {
        history.push(`/show-institution/${id}`);
    }

    function favorite() {
        setIsFavorite(!isFavorite)
    }
    
    return (
        <div className="institutionBox">
            <Button className="openInstitution" onClick={() => openInstitution(props.institutionId)}>
                    <div>{props.name}</div>
                    <div className="icons">
                        <img style={{display: `${!props.inNatura ? 'none' : ''}`}} src={InNaturaLogo}/>
                        <img style={{display: `${!props.minimamente ? 'none' : ''}`}} src={MinimamenteProcessadosLogo}/>
                        <img style={{display: `${!props.processados? 'none' : ''}`}} src={ProcessadosLogo}/>
                        <img style={{display: `${!props.ultra? 'none' : ''}`}} src={UltraprocessadosLogo}/>
                    </div>
            </Button>
            <FavoriteInstitutionButton reload="true" name={props.name} customStyle={favoriteButtonStyle} instId={props.institutionId}/>
        </div>
    )
}