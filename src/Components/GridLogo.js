import * as React from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core'

//MUI COMPONENTS
import Grid from '@mui/material/Grid'
import IconButton from '@material-ui/core/IconButton';
import Typography from '@mui/material/Typography'

//ICONS
import ArrowBackIcon from '@material-ui/icons/ArrowBack';



const useStyles = makeStyles({
    gridHeader: {
        borderEndStartRadius: '55%',
        borderEndEndRadius: '55%',
        height: '30vh',
        boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
        backgroundImage: 'linear-gradient(#FF9900, #FDC97A)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    marginTop: {
        marginTop: '5%',
        justifyContent: 'left',
        alignItems: 'left',
    },
    marginItems: {
        marginTop: '5%',
        marginBottom: '5%',
        
    },
    button: {
        position: 'absolute',
        left: 0,
        top: 0,
        zIndex: 500,
    },
    overlap: {
        position: 'absolute',
        left: 0,
        top: 0,
        zIndex: 500,
    },
    
})



export default function GridHeader(props) {
    const classes = useStyles()

    return (
        <Grid className={classes.gridHeader}>
            {props.children}
        </Grid> 
    );
}