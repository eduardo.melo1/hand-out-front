import { Height } from '@material-ui/icons'
import { width } from '@mui/system'
import React from 'react'
import '../Styles/RoundDiv.css'

export default function RoundDiv(props) {
    return (
        <div className={`container ${props.disabled ? 'disabled' : ''}`} style={{width: `${props.size || '100px'}`, height:`${props.size || '100px'}`}}>
            {props.children}
        </div>
    )
}