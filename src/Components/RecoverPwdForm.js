import React, { useState } from 'react'
import { useForm } from "react-hook-form"
import '../Styles/FormStyles.css'

export default function RecoverPwdForm({ requestRecovery, error }) {

    const [loading, setLoading] = useState(false)

    const { register, handleSubmit, formState: { errors }} = useForm();
    const onSubmit = data => {
        setLoading(true);
        requestRecovery(data).then(value =>{

            setLoading(false);
        })
    };


    return (
        <div class="form-inner">

            <div className="form-group">
                <h1 class="title">Informe seu email para recuperação de conta</h1>
                <input name="email" type="text" placeholder="Email" {...register('email', { required: true })} />
                <div class="errors">
                    {(errors.email) ? (
                        errors.email.type === 'required' ? <small>* O campo email é de Preenchimento Obrigatório</small> : ''
                    ) : ""}   
                </div> 

            </div>

            <div className="form-group">

                <input disabled={loading} className="submit" type="submit" value="Recuperar Senha" onClick={handleSubmit(onSubmit)} />
                <a class="center recover-pw-anchor" href="login">Retornar ao Login</a>

            </div>


            <h4 class="center footer-message">Ainda não tem uma conta?<a class="register-anchor" href="register">Cadastre-se</a></h4>
        </div>
    )
}