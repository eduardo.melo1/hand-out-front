import { Height } from '@material-ui/icons'
import { width } from '@mui/system'
import React from 'react'
import RoundDiv from '../RoundDiv'
import '../../Styles/RoundDiv.css'
import InNaturaLogo from '../../assets/InNatura.png'

export default function InNatura(props) {
    return (
        <RoundDiv disabled={props.disabled} size={props.size || '100px'}>
            <img src={InNaturaLogo}/>
            <p>In Natura</p>
        </RoundDiv>
    )
}