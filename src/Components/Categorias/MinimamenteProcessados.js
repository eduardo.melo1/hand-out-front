import { Height } from '@material-ui/icons'
import { width } from '@mui/system'
import React from 'react'
import RoundDiv from '../RoundDiv'
import '../../Styles/RoundDiv.css'
import MinimamenteProcessadosLogo from '../../assets/MinimamenteProcessados.png'

export default function MinimamenteProcessados(props) {
    return (
        <RoundDiv disabled={props.disabled} size={props.size || '100px'}>
            <img src={MinimamenteProcessadosLogo}/>
            <p>Minimamente <b/>
            Processados</p>
        </RoundDiv>
    )
}