import React from 'react'
import '../../Styles/CategoriasGroup.css'
import DialogNatura from './DialogNatura';
import DialogUltra from './DialogUltra';
import DialogMini from './DialogMini';
import DialogProcessados from './DialogProcessados';

export default function CategoriaGroup(props) {
    const size = '22.5vw';

    return (
        <div>
            <div className="groupCategorias">
                <DialogUltra hide={!(!props.ultra || !!props.ultra.length)} size={size} necessitiesList={props.ultra}></DialogUltra>
                <DialogProcessados hide={!(!props.processados || !!props.processados.length)} size={size} necessitiesList={props.processados}></DialogProcessados>
            </div>
            <div className="groupCategorias">
                <DialogMini hide={!(!props.minimamente || !!props.minimamente.length)} size={size} necessitiesList={props.minimamente}></DialogMini>
                <DialogNatura hide={!(!props.inNatura || !!props.inNatura.length)} size={size} necessitiesList={props.inNatura}></DialogNatura>
            </div>
        </div>
    )
}