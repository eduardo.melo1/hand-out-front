import React from 'react'
import RoundDiv from '../RoundDiv'
import MinimamenteProcessadosLogo from '../../assets/MinimamenteProcessados.png'
import MinimamenteProcessados from './MinimamenteProcessados'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';
import '../../Styles/DialogCategorias.css'

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
  
    return (
      <DialogTitle sx={{ m: 0, p: 2, background: '#FF9900', margin: '0 0 10px 0' }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.black,
              filter: 'none',
            }}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };

  BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

export default function DialogMini(props) {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    return (
        <div style={{display: `${props.hide ? 'none' : ''}`}}>
            <div onClick={handleClickOpen}>
                <MinimamenteProcessados size={props.size || '100px'} ></MinimamenteProcessados>
            </div>
            <Dialog
                style={{
                    zIndex:100000,
                }}
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                       Aceita Doações de:
                </BootstrapDialogTitle>
                <DialogContent>
                    <div className='donationTitle'>
                        <img src={MinimamenteProcessadosLogo}/>
                        <p style={{fontSize: '25px'}}>
                            Produtos Minimamente Processados
                        </p>
                    </div>
                    <DialogContentText id="alert-dialog-slide-description">
                        São produtos que são submetidos a algum processo, mas que não envolvam agregação de substâncias ao alimento original, como limpeza, moagem e pasteurização. <br/>
                        Necessidades Atualmente:
                        <ul>
                          {props.necessitiesList}
                        </ul>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                </DialogActions>
            </Dialog>
        </div>
    )
}