import { Height } from '@material-ui/icons'
import { width } from '@mui/system'
import React from 'react'
import RoundDiv from '../RoundDiv'
import '../../Styles/RoundDiv.css'
import UltraprocessadosLogo from '../../assets/Ultraprocessados.png'

export default function Ultraprocessados(props) {
    return (
        <RoundDiv disabled={props.disabled} size={props.size || '100px'}>
            <img src={UltraprocessadosLogo}/>
            <p>Ultraprocessados</p>
        </RoundDiv>
    )
}