import React from 'react'
import '../../Styles/CategoriasRow.css'
import Ultraprocessados from './Ultraprocessados'
import Processados from './Processados'
import MinimamenteProcessados from './MinimamenteProcessados'
import InNatura from './InNatura'

export default function CategoriaRow(props) {
    const size = '22.5vw';
 
    return (
        <div className="filterRow">
            <Ultraprocessados disabled={props.ultra} size={size}></Ultraprocessados>
            <Processados disabled={props.minimamente} size={size}></Processados>
            <MinimamenteProcessados disabled={props.processados} size={size}></MinimamenteProcessados>
            <InNatura disabled={props.inNatura} size={size}></InNatura>
        </div>
    )
}