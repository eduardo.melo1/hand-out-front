import { Height } from '@material-ui/icons'
import { width } from '@mui/system'
import React from 'react'
import RoundDiv from '../RoundDiv'
import '../../Styles/RoundDiv.css'
import ProcessadosLogo from '../../assets/Processados.png'

export default function Processados(props) {
    return (
        <RoundDiv disabled={props.disabled} size={props.size || '100px'}>
            <img src={ProcessadosLogo}/>
            <p>Processados</p>
        </RoundDiv>
    )
}