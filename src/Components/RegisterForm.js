import React, { useState } from 'react'
import { useForm } from "react-hook-form"
import '../Styles/FormStyles.css'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { FormControlLabel, Switch } from '@mui/material';
const eye = <FontAwesomeIcon icon={faEye} />;

function RegisterForm({ Register}) {

    const [loading, setLoading] = useState(false)
    const [apiErrors, setApiErrors] = useState({});
    const [passwordShown, setPasswordShown] = useState(false);
    const [isInstituicao, setIsInstituicao] = useState(false);

    const { register, getValues, handleSubmit, formState: { errors }} = useForm();
    const onSubmit = data => {
        setApiErrors({});
        setLoading(true);
        Register({...data, isInstituicao}).then(value =>{
            if(value)
                setApiErrors(value.data);
            else{
                value = {code: -1};
                setApiErrors(value)
            }

            setLoading(false);
        })

    };

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    function toggleSwitch (){
        setIsInstituicao(isInstituicao ? false : true);
    }


    return (
            <div class="form-inner">
                
                <div className="form-group">
                    <div class="center">
                        <FormControlLabel 
                            control={<Switch color="warning" value={isInstituicao} onChange={toggleSwitch}/>}
                            label={<small class='switch-text'>Sou uma instituição</small>}
                            labelPlacement="start"
                        />
                    </div>
                    <input name="nome" type="text" placeholder={isInstituicao ? 'Nome Instituição' : 'Nome'} {...register('nome', { required: true })} />
                    <div class="errors">
                        {(errors.nome) ? (
                            errors.nome.type === 'required' ? <small>* O campo nome é de Preenchimento Obrigatório</small> : ''
                        ) : ""}   
                    </div> 
 
                    <input name="email" type="text" placeholder="Email" {...register('email', { required: true })} />
                    <div class="errors">
                        {(errors.email) ? (
                            errors.email.type === 'required' ? <small>* O campo email é de Preenchimento Obrigatório</small> : ""
                        ) : ""}
                    </div> 
                    <div class="errors">
                        {(apiErrors) ? (
                            apiErrors.code === 5 ? <small>* Já existe uma conta cadastrada nesse email</small> : 
                            apiErrors.code === 7 ? <small>* O email inserido é invalido</small> : "") : ""
                        }   
                    </div> 

                    <div class="pass-wrapper">
                        <input name="password" type={passwordShown ? "text" : "password"} placeholder="Senha" class="center" {...register('senha', { required: true, minLength: 8, maxLength: 12 })} />
                        <i onClick={togglePasswordVisiblity}>{eye}</i>
                    </div>
                    <div class="errors"> 
                        {(errors.senha) ? (
                            errors.senha.type === 'required' ? <small>* O campo senha é de Preenchimento Obrigatório</small> :
                            errors.senha.type === 'minLength' || errors.senha.type === 'maxLength'  ? <small>* O campo senha deve ter no mínimo 8 e no máximo 12 caracteres</small> : ''
                        ) : ""}   
                    </div> 

                    <div class="pass-wrapper">
                        <input name="comfirmPwd" type={passwordShown ? "text" : "password"} placeholder="Confirmar Senha" class="center" {...register('confirmPwd', { required: true, validate: (value) => value === getValues('senha')})} />
                        <i onClick={togglePasswordVisiblity}>{eye}</i>
                    </div>
                    <div class="errors"> 
                        {(errors.confirmPwd) ? (
                            errors.confirmPwd.type === 'required' ? <small>* O campo confirmar senha é de Preenchimento Obrigatório</small> :
                            errors.confirmPwd.type === 'validate' || errors.confirmPwd.type === 'maxLength'  ? <small>* O campo confirmar senha e senha devem ser iguais</small> : ''
                        ) : ""}   
                    </div> 

                </div>

                <div className="form-group">
                    <div class="errors">
                        {(apiErrors !== {}) ? (
                            apiErrors.code === -1 ? <small>* Ocorreu um erro inesperado, tente novamente mais tarde</small> : ""
                        ) : ""}
                    </div>

                    <input disabled={loading} className="submit" type="submit" value="Cadastrar" onClick={handleSubmit(onSubmit)} />
                </div>

                <h4 class="center footer-message">Já tem uma conta?<a class="register-anchor" href="login">Faça login</a></h4>
            </div>
    )
}

export default RegisterForm
