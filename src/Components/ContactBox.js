import { Button } from '@material-ui/core';
import React from 'react'
import '../Styles/ContactBox.css'

import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';

export default function ContactBox(props) {
    const size = '22.5vw';
 
    return (
        <div className="contact">
            <Button className="buttonContact" startIcon={<LocalPhoneIcon style={{filter: 'none'}}/>}>
                {props.text}
            </Button>
        </div>
    )
}