import React, { useState } from 'react'
import { useForm } from "react-hook-form"
import '../Styles/FormStyles.css'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
const eye = <FontAwesomeIcon icon={faEye} />;


export default function LoginForm({ Login, error }) {

    const [loading, setLoading] = useState(false)
    const [apiErrors, setApiErrors] = useState({});
    const [passwordShown, setPasswordShown] = useState(false);

    const { register, handleSubmit, formState: { errors }} = useForm();
    const onSubmit = data => {
        setApiErrors({});
        setLoading(true);
        Login(data).then(value =>{
            if(value)
                setApiErrors(value.data);
            else{
                value = {code: -1};
                setApiErrors(value)
            }

            setLoading(false);
        })
    };

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };


    return (
        <div class="form-inner">

            <div className="form-group">
                <input name="email" type="text" placeholder="Email" {...register('email', { required: true })} />
                <div class="errors">
                    {(errors.email) ? (
                        errors.email.type === 'required' ? <small>* O campo email é de Preenchimento Obrigatório</small> : ''
                    ) : ""}   
                </div> 

                <div class="pass-wrapper">
                    <input name="senha" type={passwordShown ? "text" : "password"} placeholder="Senha" class="center" {...register('senha', { required: true, minLength: 8, maxLength: 12 })} />
                    <i onClick={togglePasswordVisiblity}>{eye}</i>
                </div>
                <div class="errors"> 
                    {(errors.senha) ? (
                        errors.senha.type === 'required' ? <small>* O campo senha é de Preenchimento Obrigatório</small> :
                        errors.senha.type === 'minLength' || errors.senha.type === 'maxLength'  ? <small>* O campo senha deve ter no mínimo 8 e no máximo 12 caracteres</small> : ''
                    ) : ""}   
                </div> 

            </div>

            <div className="form-group">
                <div class="errors">
                    {(apiErrors !== {}) ? (
                        (apiErrors.code === 7 ||  apiErrors.code === 8) ? <small>* As credencias inseridas são inválidas ou não existem no nosso sistema</small> :
                        apiErrors.code === 9 ? <small>* Você precisa verificar seu email antes de acessar sua conta</small> :
                        apiErrors.code === -1 ? <small>* Ocorreu um erro inesperado, tente novamente mais tarde</small> : ""
                    ) : ""}
                </div> 

                <input disabled={loading} className="submit" type="submit" value="Fazer Login" onClick={handleSubmit(onSubmit)} />
                <a class="center recover-pw-anchor" href="recover">Esqueceu a senha?</a>
                

            </div>


            <h4 class="center footer-message">Ainda não tem uma conta?<a class="register-anchor" href="register">Cadastre-se</a></h4>
        </div>
    )
}