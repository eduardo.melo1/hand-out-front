import React, { useEffect } from 'react'
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarIcon from '@mui/icons-material/Star';
import { useHistory } from 'react-router';
import { Favorite } from '@material-ui/icons';
import { favoriteInstitution, getFavorites, unfavoriteInstitution } from '../api/PageInfoController';
import { getUserId } from '../api/AccountController';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';
import { Fab } from '@material-ui/core';
import '../Styles/FavoriteDialog.css'

const fabStyle = {
    position: 'absolute',
    top: 8,
    right: 8,
    zIndex: 999,
    boxShadow: 'none',
    backgroundColor: 'transparent'
};

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
  
    return (
      <DialogTitle sx={{ m: 0, p: 2, background: '#FF9900', margin: '0 0 10px 0' }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.black,
              filter: 'none',
            }}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };

  BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };

export default function FavoriteInstitutionButton(props) {
    const history = useHistory();
    const [favorites, setFavorites] = React.useState();
    const [isFavorite, setIsFavorite] = React.useState(false);
    const [open, setOpen] = React.useState(false);


    const size = '22.5vw';

    useEffect(() => {

        document.title = 'Detalhes Instituição';

        updateFavoriteData()
    }, [props.instId])

    async function updateFavoriteData(){
        const userId = getUserId()
        if(userId){
            const response = await getFavorites(userId);
            setFavorites(response.favorites)
            updateIsFavorite(response.favorites)
        }   
    }

    function updateIsFavorite(favList){
        
        if(!props.instId || !favList){
            setIsFavorite(false);
            return;
        }
        for (const fav in favList) {
            if (favList.hasOwnProperty.call(favList, fav)) {
                const instId = favList[fav].id_fav;

                if(instId == props.instId){
                    setIsFavorite(true);
                    return;
                }
                
            }
        }
    }

    async function switchFavorite(){
        if(isFavorite)
            handleClickOpen();
        else
            favorite();
    }

    async function favorite() {
        const userId = getUserId()
        if(userId && props.instId){
            setIsFavorite(true);
            const response = await favoriteInstitution(userId, props.instId);
            if(!response || response.status >= 400){
                setIsFavorite(false);
            }
        }

        if(props.reload)
            window.location.reload(true)
    }

    async function deleteFavorite() {
        const userId = getUserId()
        if(userId && props.instId){
            setIsFavorite(false);
            const response = await unfavoriteInstitution(userId, props.instId);
            if(!response || response.status >= 400){
                setIsFavorite(true);
            }
        }
        handleClose();
        if(props.reload)
            window.location.reload(true)
    }
 
    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    return (
        <div>
            <div onClick={switchFavorite}>
                <Fab style={props.customStyle ? props.customStyle : fabStyle} aria-label={'abrir barra de navegação'}>{isFavorite ? <StarIcon  style={{filter: 'none'}}/> : <StarBorderIcon  style={{filter: 'none'}}/>}</Fab>
            </div>
            <Dialog
                style={{
                    zIndex:100000,
                }}
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                       Confirmar Ação
                </BootstrapDialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        Deseja excluir {props.name ? props.name : 'esta instituição'} de suas instituições favoritas?
                    </DialogContentText>
                </DialogContent>
                <DialogActions className="confirmDialog">
                    <Button className="confirmButton orange" autoFocus onClick={handleClose}>
                        Não
                    </Button>
                    <Button className="confirmButton white" onClick={deleteFavorite}>
                        Sim
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}