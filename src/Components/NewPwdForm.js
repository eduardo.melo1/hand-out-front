import React, { useState } from 'react'
import { useForm } from "react-hook-form"
import '../Styles/FormStyles.css'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
const eye = <FontAwesomeIcon icon={faEye} />;

function NewPwdForm({ NewPwd}) {

    const [loading, setLoading] = useState(false)
    const [apiErrors, setApiErrors] = useState({});
    const [passwordShown, setPasswordShown] = useState(false);

    const { register, getValues, handleSubmit, formState: { errors }} = useForm();
    const onSubmit = data => {
        setApiErrors({});
        setLoading(true);
        NewPwd({...data}).then(value =>{

            if(value)
                setApiErrors(value.data);
            else{
                value = {code: -1};
                setApiErrors(value)
            }

            setLoading(false);
        })

    };

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    return (
            <div class="form-inner">
                
                <div className="form-group">

                    <div class="pass-wrapper">
                        <input name="password" type={passwordShown ? "text" : "password"} placeholder="Nova Senha" class="center" {...register('senha', { required: true, minLength: 8, maxLength: 12 })} />
                        <i onClick={togglePasswordVisiblity}>{eye}</i>
                    </div>
                    <div class="errors"> 
                        {(errors.senha) ? (
                            errors.senha.type === 'required' ? <small>* O campo nova senha é de Preenchimento Obrigatório</small> :
                            errors.senha.type === 'minLength' || errors.senha.type === 'maxLength'  ? <small>* O campo senha deve ter no mínimo 8 e no máximo 12 caracteres</small> : ''
                        ) : ""}   
                    </div> 

                    <div class="pass-wrapper">
                        <input name="comfirmPwd" type={passwordShown ? "text" : "password"} placeholder="Confirmar Senha" class="center" {...register('confirmacaoSenha', { required: true, validate: (value) => value === getValues('senha')})} />
                        <i onClick={togglePasswordVisiblity}>{eye}</i>
                    </div>
                    <div class="errors"> 
                        {(errors.confirmacaoSenha) ? (
                            errors.confirmacaoSenha.type === 'required' ? <small>* O campo confirmar senha é de Preenchimento Obrigatório</small> :
                            errors.confirmacaoSenha.type === 'validate' || errors.confirmacaoSenha.type === 'maxLength'  ? <small>* O campo confirmar senha e senha devem ser iguais</small> : ''
                        ) : ""}   
                    </div> 

                </div>

                <div className="form-group">
                    <div class="errors">
                        {(apiErrors !== {}) ? (
                           apiErrors.code === 4 ? <small>* O token de redefinição é invalido ou expirou</small> :
                           apiErrors.code === -1 ? <small>* Um erro inesperado aconteceu, solicite uma nova redefinição</small> : ""
                            
                        ) : ""}
                    </div>  

                    <input disabled={loading} className="submit" type="submit" value="Atualizar Senha" onClick={handleSubmit(onSubmit)} />
                </div>

                <h4 class="center footer-message">Já tem uma conta?<a class="register-anchor" href="/login">Faça login</a></h4>
            </div>
    )
}

export default NewPwdForm
