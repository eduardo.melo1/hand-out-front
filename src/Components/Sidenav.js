import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import HouseIcon from '@mui/icons-material/House';
import StarIcon from '@mui/icons-material/Star';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import { Fab } from '@mui/material';
import { styled } from '@material-ui/core';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';


const fabStyle = {
    position: 'absolute',
    top: 16,
    left: 16,
    zIndex: 999,
    boxShadow: 'none',
    backgroundColor: 'transparent'
};

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-start',
}));

export default function SideNav() {
  const [state, setState] = React.useState({
    isOpen: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {['Tela inicial', 'Instituições Favoritadas', 'Dados Cadastrais', 'Fazer LogOut'].map((text, index) => (
          <ListItem button key={text}
            component="a" 
            href={
                index === 0 ? "/" :
                index === 1 ? "/favorites" :
                index === 2 ? "/meuCadastro" :
                index === 3 ? "/logout" : "/logout"
            }
          >
            <ListItemIcon>
              {
              index === 0 ? <HouseIcon style={{filter: 'none'}}/> :
              index === 1 ? <StarIcon style={{filter: 'none'}}/> :
              index === 2 ? <PersonOutlineIcon style={{filter: 'none'}}/> :
              index === 3 ? <LogoutIcon style={{filter: 'none'}}/> : <a/>
              }
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
        <React.Fragment key={'left'}>
            
          <Fab sx={fabStyle} aria-label={'abrir barra de navegação'}onClick={toggleDrawer('left', true)}><MenuIcon  style={{filter: 'none'}}/></Fab>
          <Drawer
            anchor={'left'}
            open={state['left']}
            onClose={toggleDrawer('left', false)}
          >
            <DrawerHeader>
              <IconButton onClick={toggleDrawer('left', false)}>
                <ArrowBackIcon  style={{filter: 'none'}}/>
              </IconButton>
            </DrawerHeader>
            <Divider />
            {list('left')}
          </Drawer>
        </React.Fragment>
    </div>
  );
}