import React from 'react'
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Fab } from '@material-ui/core'
import { useHistory } from 'react-router';

const fabStyle = {
    position: 'absolute',
    top: 8,
    left: 8,
    zIndex: 999,
    boxShadow: 'none',
    backgroundColor: 'transparent'
};

export default function ReturnArrow(props) {
    const history = useHistory();
 
    const size = '22.5vw';

    function returnToMain () {
        history.push('/map');
    }
 
    return (
        <div>
            <Fab style={fabStyle} aria-label={'abrir barra de navegação'}onClick={returnToMain}><ArrowBackIcon  style={{filter: 'none'}}/></Fab>
        </div>
    )
}