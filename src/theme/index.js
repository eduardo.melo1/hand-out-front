import baseStyle from "./base"

const themeStyle = {
  ...baseStyle,
  overrides: {
    fontSize: 14,
    MuiButton: {
      root: {
        border: 0,
        borderRadius: 10,
        boxShadow: 'none',
        fontSize: 12
      },
      contained: {
        boxShadow: 'none',
        minWidth: 100
      },
      containedPrimary: {
        color: baseStyle.palette.primary.contrastText,
        backgroundColor: baseStyle.palette.primary.main,
        '&:hover': {
          backgroundColor: baseStyle.palette.primary.dark,
          color: baseStyle.palette.primary.contrastText,
          '@media (hover: none)': {
            backgroundColor: baseStyle.palette.primary.main,
            color: baseStyle.palette.primary.contrastText,
          },
        },
        '&:focus': {
          outline: 'none'
        }
      },
      outlined: {
        color: 'rgba(0, 0, 0, 0.4)',
        minWidth: 100
      },
      outlinedPrimary: {
        color: baseStyle.palette.primary.main,
      },
      outlinedSecondary: {
        backgroundColor: '#FFFFFF',
      }
    },
    MuiButtonBase: {
      root: {
        boxShadow: '0',
        '&:focus': {
          outline: 'none',
        }
      },
    },
    MuiListItemIcon: {
      root: {
        '&:focus': {
          outline: 'none',
        }
      },
    },
    MuiPaper: {
      root: {
        boxShadow: '0',
        margin: '15px 0',
        display: 'flex',
        flexDirection: 'column',
        borderEndStartRadius: '35%',
        borderEndEndRadius: '35%',
        height: '22vh',
        boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
        backgroundImage: 'linear-gradient(#FF9900, #FDC97A)',
      },
      elevation1: {
        boxShadow: '0',
      },
      elevation2: {
        boxShadow: '0',
      }
    },
    MuiGrid: {
      root: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: baseStyle.palette.text.white,
      },
    },
    MuiTypography: {
      h1: {
        fontSize: 24,
        color: '#111A2B',
        letterSpacing: 0,
        margin: '20px 0'
      },
      h2: {
        display: 'flex',
        alignItems: 'center',
        fontSize: 18,
        color: '#000000',
        letterSpacing: 0
      },
      h3: {
        display: 'flex',
        alignItems: 'center',
        fontSize: 16,
        color: '#000000',
        letterSpacing: 0,
        textTransform: 'uppercase'
      },
      h4: {
        fontSize: 14,
        letterSpacing: 0,
        textTransform: 'uppercase'
      }
    },
    MuiInputBase: {
      root: {
        fontSize: 14
      }
    },
    MuiInputLabel: {
      root: {
        fontSize: 14,
        color: '#000'
      }
    },
    MuiAppBar: {
      colorPrimary: {
        color: baseStyle.palette.primary.main,
        backgroundColor: baseStyle.palette.text.white,
      }
    },
    MuiDrawer: {
      paperAnchorDockedLeft: {
        border: 0,
      }
    },    
    MuiTabs: {
      indicator: {
        backgroundColor: baseStyle.palette.primary.main,
      },
    },
    MuiTab: {
      root: {
        maxWidth: 'unset',
        "&:hover": {
          backgroundColor: baseStyle.palette.primary.main,
          color: "#fff",
        },
        selected: {
          backgroundColor: baseStyle.palette.primary.main,
          color: "#fff",
          "&:hover": {
            backgroundColor: baseStyle.palette.primary.main,
            color: "#fff",
          }
        }
      }
    },
    MuiTable: { 
      root: {
        width: '100%',
      }
    },
    MuiTableHead: {
      root: {
        backgroundColor: '#fff'
      }
    },
    MuiTableCell: {
      root: {
        padding: 10,
        '&:last-child': {
          paddingRight: 10
        }
      },
      head: {
        color: '#121B2C',
        fontSize: 14,
        fontWeight: 500
      }
    },
    MuiCardContent: {
      root: {
        '&:last-child': {
          
        }
      }
    }
  }
}

export default themeStyle;